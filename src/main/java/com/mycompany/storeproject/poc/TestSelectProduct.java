/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;
/**
 *
 * @author user
 */
public class TestSelectProduct {
    public static void main(String[] args){
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //process here
        try {
            String sql = "SELECT id,name,price FROM product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                Product product = new Product(id,name,price);
                System.out.println(product);
                //System.out.println(id+" "+name+" "+price);
            }
        } catch (SQLException ex) {
            System.out.println("Error: SQL error by someting :"+ex);
        }
        db.close();
    }
}
