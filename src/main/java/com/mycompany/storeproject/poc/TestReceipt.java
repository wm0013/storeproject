/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author user
 */
public class TestReceipt {
    public static void main(String[] args) {
        Product p1 = new Product(1, "Cold Tea", 30);
        Product p2 = new Product(1, "Americano", 40);
        User seller = new User("Naded", "88888888", "password");
        Customer customer = new Customer("Pawit", "99999999");
        Receipt receipt  = new Receipt(seller,customer);
        receipt.addReciptDetail(p1, 1);
        receipt.addReciptDetail(p2, 3);
        System.out.println(receipt);
        receipt.deletelReceipDetail(0);
        System.out.println(receipt);
        receipt.addReciptDetail(p1, 2);
        System.out.println(receipt);
        receipt.addReciptDetail(p1, 2);
        System.out.println(receipt);
    }
}
